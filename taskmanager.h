#ifndef TASKMANAGER_H
#define TASKMANAGER_H

#include <QObject>
#include <QQueue>
#include <QThread>
#include <QSemaphore>

class BaseTask;

class TaskManager : public QThread{
    Q_OBJECT
public:
    QQueue<BaseTask*> taskQueue;
    QSemaphore* semaphore;
    TaskManager();
    void run() override;
public:
    void handleResults(BaseTask*);
};

#endif // TASKMANAGER_H

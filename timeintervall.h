#ifndef TIMEINTERVALL_H
#define TIMEINTERVALL_H


class TimeIntervall{

public:
    TimeIntervall();
    static int every_nSeconds(int seconds){
        return seconds*1000;
    }

    static int every_nMinutes(int minutes){
        return minutes*every_nSeconds(60);
    }

    static int every_nHours(int hours){
        return hours*every_nMinutes(60);
    }

    static int every_nDays(int days){
        return days*every_nHours(24);
    }


};

#endif // TIMEINTERVALL_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QSystemTrayIcon;
class QMenu;
class IconMenu;
class TaskManager;

class MainWindow : public QMainWindow{
    Q_OBJECT

public:    
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QSystemTrayIcon* getSysTrainIcon();
    void createActionNewTask1_OnTaskMenu(QMenu* tasksSubMenu,const char actionName[]);
    void createActionNewTask2_OnTaskMenu(QMenu* tasksSubMenu,const char actionName[]);
    void createActionExit_OnMenu(QMenu* tasksSubMenu,const char actionName[]);
    void setTaskManager(TaskManager *taskManager);
    void printActionName(const char actionName[]);
    TaskManager *getTaskManager() const;

private:
    Ui::MainWindow* ui;
    QSystemTrayIcon* sysTrainIcon;
    IconMenu* iconMenu;
    QAction* newTaskAction;
    TaskManager* taskManager;
    QMenu* createIconMenu();
    void newTask1_actionHandler();
    void newTask2_actionHandler();
    void createTask1(QString text);
    void createTask2(QString fileName);
    int viewExitDialog();

};

#endif // MAINWINDOW_H

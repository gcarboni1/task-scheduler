#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "taskmanager.h"
#include "task.h"
#include <QMessageBox>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QObject>
#include <iostream>
#include <functional>
#include <QInputDialog>
#include <QDir>
#include <QTimer>
#include <QSpacerItem>
#include <QGridLayout>
#include <string>
#include <QDebug>


constexpr char RUN_LABEL[]="Run";
constexpr char NEW_TASK1_LABEL[]="New Task 1";
constexpr char NEW_TASK2_LABEL[]="New Task 2";
constexpr char EXIT_STRING[]="Exit";


MainWindow::MainWindow(QWidget *parent) :QMainWindow(parent),ui(new Ui::MainWindow){
    ui->setupUi(this);
    sysTrainIcon = new QSystemTrayIcon(this);
    sysTrainIcon->setIcon(QIcon(":/icon.png"));
    sysTrainIcon->setToolTip("Task Scheduler");
    sysTrainIcon->setContextMenu(createIconMenu());
    sysTrainIcon->setVisible(true);
    sysTrainIcon->showMessage(tr("Task Scheduler"),tr("Application Started"));
}

MainWindow::~MainWindow(){
    delete ui;
    delete sysTrainIcon;
}

QSystemTrayIcon* MainWindow::getSysTrainIcon(){
    return sysTrainIcon;
}

void MainWindow::setTaskManager(TaskManager *taskManager){
    this->taskManager = taskManager;
}

TaskManager *MainWindow::getTaskManager() const{
    return taskManager;
}



/****************************************************************************
 *  MENU CONFIGURATION
 */

QMenu* MainWindow::createIconMenu(){
    QMenu* qmenu = new QMenu();
    createActionExit_OnMenu(qmenu, EXIT_STRING);
    QMenu* taskSubMenu = new QMenu(RUN_LABEL);
    createActionNewTask1_OnTaskMenu(taskSubMenu, NEW_TASK1_LABEL);
    createActionNewTask2_OnTaskMenu(taskSubMenu, NEW_TASK2_LABEL);
    qmenu->addMenu(taskSubMenu);
    return qmenu;
}






/****************************************************************************
 *  CREATE MENU ACTIONS
 */

void MainWindow::printActionName(const char actionName[]){
    qDebug() << actionName <<" action called";
}

void MainWindow::createActionNewTask1_OnTaskMenu(QMenu* tasksSubMenu,const char actionName[]){
    QAction* newTask1Act = new QAction(QObject::tr(actionName));
    connect(newTask1Act, &QAction::triggered, [this,actionName](){
        printActionName(actionName);
        newTask1_actionHandler();
    });
    tasksSubMenu->addAction(newTask1Act);
}

void MainWindow::createActionNewTask2_OnTaskMenu(QMenu *tasksSubMenu, const char actionName[]){
    QAction* newTask2Act = new QAction(QObject::tr(actionName));
    connect(newTask2Act, &QAction::triggered, [this,actionName](){
        printActionName(actionName);
        newTask2_actionHandler();
    });
    tasksSubMenu->addAction(newTask2Act);
}


void MainWindow::createActionExit_OnMenu(QMenu* qmenu, const char actionName[]){
    QAction* newTask1Act = new QAction(QObject::tr(actionName));
    connect(newTask1Act, &QAction::triggered, [this,actionName](){
        printActionName(actionName);
        if (viewExitDialog() == QMessageBox::Ok){
            taskManager->taskQueue.clear();
            taskManager->quit();
            QCoreApplication::quit();
        }
    });
    qmenu->addAction(newTask1Act);
}

int MainWindow::viewExitDialog(){
    QMessageBox msgBox;
    msgBox.setText("Close the application means to close all the tasks in background.");
    msgBox.setInformativeText("Do you want to Exit?");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    int ret = msgBox.exec();
    return ret;
}






/**********************************************************
 *  TASK ACTION HANDLER
 */

void MainWindow::newTask1_actionHandler(){
    bool ok;
    QString textToPrint = QInputDialog::getText(this, tr("Ti-Care"),
        tr("Insert Text:"), QLineEdit::Normal,"", &ok);

    if (ok && !textToPrint.isEmpty()){
        qDebug() << "input from dialog: " << textToPrint.toUtf8().begin();
        new Task1(this,textToPrint);
    }else{
        sysTrainIcon->showMessage(tr("Task Scheduler"),tr("No input available"));
    }
}

void MainWindow::newTask2_actionHandler(){
    QString filePath="C:\\Users\aslab\\CPP\\QT-CPP\\ti-care-scheduler\\ti-care\\resources\\icon.ico";
    new Task2(this,filePath);
}









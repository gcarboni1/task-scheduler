#include "task.h"
#include "timeintervall.h"
#include <iostream>
#include <deque>
#include <iterator>
#include <QFileInfo>
#include <QTimer>
#include <functional>
#include <QSystemTrayIcon>
#include <QObject>
#include <QThread>
#include <QTime>
#include <QDebug>


BaseTask::BaseTask(MainWindow* mainWindow){
    this->timer = new QTimer(this);
    this->mainWindow= mainWindow;
    this->taskManager = mainWindow->getTaskManager();
    this->setTimerOn();
}

int BaseTask::getMilliseconds(){
    return this->milliseconds;
}

void BaseTask::setMilliseconds(int millisecond) {
    this->milliseconds= millisecond;
}

void BaseTask::setMainWindow(MainWindow(* mainWindow)){
    this->mainWindow=mainWindow;
}

void BaseTask::setTimerOn(){
    timer = new QTimer(this);
    timer->setInterval(getMilliseconds());
    QTimer::connect(timer, &QTimer::timeout, this, [=](){
        taskManager->taskQueue.enqueue(this);
        taskManager->semaphore->release(1);
    });
    timer->start();
}

void BaseTask::startTask(){
    mainWindow->getSysTrainIcon()->showMessage(name,"Started");
    job();
    taskManager->handleResults(this);
}

QString BaseTask::getName() const{
    return name;
}


/************************************************************************
 *                  TASK 1
 */

Task1::Task1(MainWindow* mainWindows,QString textToPrint) : BaseTask(mainWindows){
    this->name="Task 1";
    this->setObjectName("Task1");
    this->milliseconds=TimeIntervall::every_nSeconds(10);
    this->textToPrint=textToPrint;
    this->setTimerOn();
}

void Task1::setTextToPrint(QString text) {
    this->textToPrint=text;
}

void Task1::job(){
    qDebug() << textToPrint.toLocal8Bit().constData();
}



/************************************************************************
 *                  TASK 2
 */

Task2::Task2(MainWindow* mainWindows,QString filePath) : BaseTask(mainWindows){
    this->name="Task 2";
    this->setObjectName("Task2");
    this->milliseconds=TimeIntervall::every_nSeconds(30);
    this->filePath=filePath;
    this->setTimerOn();
}

void Task2::job(){
    if(! QFileInfo::exists(this->filePath)){
       qDebug() << "file does not exists";
    }else{
          qDebug() << "file exists!!!";
    }    
}


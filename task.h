#ifndef TASK_H
#define TASK_H

#include "taskmanager.h"
#include "mainwindow.h"
#include <iostream>
#include <QTimer>
#include <QObject>
#include <QQueue>
#include <QThread>
#include <QSemaphore>


class BaseTask :public QObject{

public:
    TaskManager* taskManager;
    BaseTask(MainWindow* mainWindows);

    void setMilliseconds(int millisecond);
    int getMilliseconds();    

    void setMainWindow(MainWindow* mainWindow);
    void setTimerOn();  
    QString getName() const;
    void startTask();
    void virtual job()=0;

protected:
    QString name;
    int milliseconds;
    QTimer* timer;
    MainWindow* mainWindow;
    QString result;
};



class Task1 :public BaseTask{

public:
    Task1(MainWindow* mainWindows, QString textToPrint);
    void setTextToPrint(QString text);
private:
    QString textToPrint;
    void job() override;
};





class Task2:public BaseTask{

public:
    Task2(MainWindow* mainWindows,QString textToPrint);
    void setFileNameToCheck(QString filePath);    
private:
    QString filePath;
    void job() override;
};





#endif









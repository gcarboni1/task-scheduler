#include "mainwindow.h"
#include "taskmanager.h"
#include "task.h"
#include <QApplication>
#include <iterator>


class TimeIntervall;

int main(int argc, char *argv[]){

    QApplication app(argc, argv);
    MainWindow w;

    TaskManager* taskManager = new TaskManager();
    taskManager->start();

    w.setTaskManager(taskManager);
    w.hide();

    app.setQuitOnLastWindowClosed(false);
    return app.exec();
}

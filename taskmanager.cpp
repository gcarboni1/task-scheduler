#include "taskmanager.h"
#include "task.h"
#include <QDebug>

constexpr int NUM_OF_PARALLEL_TASK=1;

TaskManager::TaskManager(){
    semaphore= new QSemaphore(NUM_OF_PARALLEL_TASK);
}


void TaskManager::run(){
    while(true){
        semaphore->acquire();
        if(!taskQueue.empty()){            
            BaseTask* baseTask = taskQueue.dequeue();
            qDebug() << baseTask->getName() << " started ";
            baseTask->startTask();
        }
    }
}


void TaskManager::handleResults(BaseTask* task){    
    qDebug() << task->getName() << " finished"  << endl;

}
